import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../auth.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userName: any;
  avatarUrl: any;

  constructor(private authService: AuthenticationService, private toastr: ToastrService, private router: Router) {
  }

  ngOnInit() {
    this.userName = localStorage.getItem('userName');
    this.avatarUrl = localStorage.getItem('url');
  }

  onLogout() {
    this.authService.logout();
    this.toastr.success('Successfully logged out!', 'Success!');
  }

  onBack() {
    this.router.navigate(['users']);
  }

}
