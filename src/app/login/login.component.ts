import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {AuthenticationService} from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private  toastr: ToastrService, private authenticationService: AuthenticationService, private activatedRoute: ActivatedRoute) {
  }

  isLoggedIn = false;

  ngOnInit() {
    const param = this.activatedRoute.snapshot.queryParams;
    let resultBody: any = {};
    resultBody = param;
    const token: any = resultBody.token;

    if (token) {
      this.isLoggedIn = true;
      localStorage.setItem('token', token);
      localStorage.setItem('isLoggedIn', 'true');
      this.toastr.success('Successfully logged in!', 'Success!');


      this.router.navigate(['/users']);
    } else {
      this.toastr.error('Login Failed. Please try again', 'Login Failed');
      this.router.navigate(['/login']);

    }

  }


}
