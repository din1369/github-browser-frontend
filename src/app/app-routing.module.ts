import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';

import {UserComponent} from './user/user.component';
import {RedirectComponent} from './redirect/redirect.component';
import {AuthGuard} from './auth-guard.service';
import {SearchComponent} from './search/search.component';


const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: RedirectComponent
  },
  // {path: 'users' , canActivate: [AuthGuard],  component:  UserComponent},
  {
    path: 'oauth2/redirect',
    component: LoginComponent
  },
  {
    path: 'users',
    component: UserComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'users/search/:userName',
    component: SearchComponent,
    canActivate: [AuthGuard]
  },
  // keep the below route at the end always
  {
    path: '**',
    redirectTo: '/login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
