import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {
  }

  public getUserRepoDetails(): Observable<any> {
    return this.http.get(environment.API.USER + '/getUserRepo');
  }

}

