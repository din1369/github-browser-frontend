import {AfterViewInit, Component, OnInit} from '@angular/core';
import {LocalDataSource, Ng2SmartTableModule} from 'ng2-smart-table';
import {UserService} from './user.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html'
})
export class UserComponent implements OnInit, AfterViewInit {
  allRepos: any[] = [];
  settings = {};
  avatarUrl: any;
  userName: any;
  searchName: any;
  display = false;

  source: LocalDataSource;

  constructor(private userService: UserService, private router: Router, private toastr: ToastrService) {

  }

  ngOnInit(): void {
    this.getUserRepoDetails();
  }


  ngAfterViewInit(): void {
  }


  getUserRepoDetails() {
    this.display = true;
    this.userService.getUserRepoDetails().subscribe(response => {
        let result = {};
        result = response;
        this.allRepos = Object.assign(result);
        this.avatarUrl = this.allRepos[0].owner[0].avatar_url;
        this.userName = this.allRepos[0].owner[0].login;
        localStorage.setItem('userName', this.userName);
        localStorage.setItem('url', this.avatarUrl);
        console.log(this.avatarUrl);
        this.source = new LocalDataSource(this.allRepos);
        this.settings = {
          columns: {
            name: {
              title: 'Repository Name'
            },
            full_name: {
              title: 'Repository Full Name'
            },
          },
          class: ['"table table-striped'],
          actions: false,
          pager: {
            display: true,
            perPage: 15
          }


        };

      this.display = false;

      }, error => {
      this.display = false;
        this.toastr.error('Failed To Load Data!', 'Error!');
      }
    );

  }

  search(form: NgForm) {
    const value = form.value;
    this.router.navigate(['users/search/' + value.searchUserName ]);
  }

}
