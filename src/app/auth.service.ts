import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable()
export class AuthenticationService {
  constructor(private router: Router) {
  }

  loggedIn = false;


  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    if (token) {
      return true;
    } else {
      return false;
    }
  }


  logout() {
    this.loggedIn = false;
    localStorage.removeItem('isLoggedIn');
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }


}
