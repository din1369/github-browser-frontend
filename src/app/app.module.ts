import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {UserComponent} from './user/user.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';

import {LoginComponent} from './login/login.component';
import {AppRoutingModule} from './app-routing.module';
import {AuthGuard} from './auth-guard.service';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {Ng2TableModule} from 'ng2-table';
import {PaginationModule} from 'ng2-bootstrap';
import {UserService} from './user/user.service';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {OwlDateTimeModule} from 'ng-pick-datetime/date-time/date-time.module';
import {OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {ConfirmationService, ConfirmDialogModule, SharedModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/components/dialog/dialog';
import {AuthenticationService} from './auth.service';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TokenInterceptor} from './token.interceptor';
import {RedirectComponent} from './redirect/redirect.component';
import {SearchComponent} from './search/search.component';
import {SearchService} from './search/search.service';
import {AtomSpinnerModule} from 'angular-epic-spinners';


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RedirectComponent,
    SearchComponent,


    // ConfirmDialogComponent

  ],

  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({
        timeOut: 10000,
        positionClass: 'toast-top-full-width',
        preventDuplicates: true,

      },
    ),
    Ng2TableModule,
    PaginationModule.forRoot(),
    Ng2SmartTableModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OwlNativeDateTimeModule,
    DialogModule,
    SharedModule,
    AngularMultiSelectModule,
    AtomSpinnerModule


  ],


  providers: [AuthenticationService, AuthGuard, UserService, ConfirmationService, SearchService , {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
