import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class SearchService {
  constructor(private http: HttpClient) {
  }

  public getSearchedUserRepoDetails(Username: any): Observable<any> {
    return this.http.get(environment.API.USER + '/searchRepo/' + Username);
  }

}
