import {Component, OnInit} from '@angular/core';
import {LocalDataSource} from 'ng2-smart-table';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {SearchService} from './search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  allRepos: any[] = [];
  settings = {};
  avatarUrl: any;
  userName: any;
  searchUserName: any;
  display = false;

  source: LocalDataSource;

  // tslint:disable-next-line:max-line-length
  constructor(private searchService: SearchService, private router: Router, private toastr: ToastrService, private activateRoute: ActivatedRoute
  ) {
  }


  ngOnInit(): void {
    this.searchUserName = this.activateRoute.snapshot.paramMap.get('userName'),
      this.getSearchedUserRepoDetails(this.searchUserName);
  }


  getSearchedUserRepoDetails(name) {
    this.display = true;
    this.searchService.getSearchedUserRepoDetails(name).subscribe(response => {
        if (response.status !== 'FAILURE') {
          let result = {};
          result = response;
          this.allRepos = Object.assign(result);
          this.avatarUrl = this.allRepos[0].owner[0].avatar_url;
          this.userName = this.allRepos[0].owner[0].login;
          console.log(this.avatarUrl);
          this.source = new LocalDataSource(this.allRepos);
          this.settings = {
            columns: {
              name: {
                title: 'Repository Name'
              },
              full_name: {
                title: 'Repository Full Name'
              },
            },
            class: ['"table table-striped'],
            actions: false,
            pager: {
              display: true,
              perPage: 15
            }


          };
          this.display = false;
        } else {
          this.display = false;
          this.toastr.error('User ' + name + ' was not found on GitHub', 'Error!');
          this.router.navigate(['users']);
        }


      }, error => {
      this.display = false;
        this.toastr.error('Failed To Load Data!', 'Error!');
      }
    );

  }


}
